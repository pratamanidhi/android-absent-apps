package com.project.android_absent.Util

import android.os.Build
import android.util.Log
import android.util.Patterns
import androidx.annotation.RequiresApi
import org.json.JSONObject
import java.text.NumberFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.OffsetDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.util.*

class Util {
    companion object {


        fun IDRFormatter(price: Double): String {
            val format = NumberFormat.getCurrencyInstance()
            format.maximumFractionDigits = 2
            format.currency = Currency.getInstance("IDR")

            return format.format(price)
        }

        fun PriceFormater(price : Int):String{
            val priceFormat = price * 14327.30
            return priceFormat.toString()
        }


        fun UTCtoNow(date : String, format : String) : String{
            val format = SimpleDateFormat(format).parse(date)
            val parsedDate_format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(format)
            val parsedDate2 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").apply {
                this.timeZone = TimeZone.getTimeZone("UTC")
            }.parse(parsedDate_format)
            val time = SimpleDateFormat("dd MMMM yyyy HH:mm", Locale.getDefault()).format(parsedDate2)
            return time
        }



        @RequiresApi(Build.VERSION_CODES.O)
        fun convertToUTC(date:String):String {
            val DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy'T'HH:mm:ss:SSSX")
            val DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date)
            val parsedDate = SimpleDateFormat("dd/MM/yyyy'T'HH:mm:ss:SSSX").format(format)
            val odtInstanceAtOffset = OffsetDateTime.parse(parsedDate, DATE_TIME_FORMATTER)
            val odtInstanceAtUTC = odtInstanceAtOffset.withOffsetSameInstant(ZoneOffset.UTC)
            val dateStringInUTC = odtInstanceAtUTC.format(DATE_FORMAT)
            Log.e("mess", dateStringInUTC)
            return dateStringInUTC
        }

        @RequiresApi(Build.VERSION_CODES.O)
        fun dateTimeNowUTC(): String{
            val current = LocalDateTime.now()
            val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
            val formatted = current.format(formatter)
            Log.e("mess", formatted)
            val DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy'T'HH:mm:ss:SSSX")
            val DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
            val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(formatted)
            val parsedDate = SimpleDateFormat("dd/MM/yyyy'T'HH:mm:ss:SSSX").format(format)
            val odtInstanceAtOffset = OffsetDateTime.parse(parsedDate, DATE_TIME_FORMATTER)
            val odtInstanceAtUTC = odtInstanceAtOffset.withOffsetSameInstant(ZoneOffset.UTC)
            val dateStringInUTC = odtInstanceAtUTC.format(DATE_FORMAT)
            return dateStringInUTC
        }

        @RequiresApi(Build.VERSION_CODES.O)
        fun getDateDiffence(startDate: String, endDate: String): Long{
            println("getting difference")
            var diff: Long = 0L
            try{
                val date1 = LocalDateTime.parse(startDate.replace(" ", "T"))
                val date2 = LocalDateTime.parse(endDate.replace(" ", "T"))
                println(date1)
                println(date2)
                diff = ((date2.toEpochSecond(ZoneOffset.UTC) * 1000) - (date1.toEpochSecond(ZoneOffset.UTC) * 1000)).toLong()
            }catch (e: ParseException){
                println(e.toString())
            }
            println(diff)
            return diff
        }

        fun getTimeParsed(timeLeft: Long): JSONObject {
            val secondsInMilli: Long = 1000
            val minutesInMilli = secondsInMilli * 60
            val hoursInMilli = minutesInMilli * 60

            val elapsedHours: Long = timeLeft / hoursInMilli
            var different = timeLeft % hoursInMilli

            val elapsedMinutes: Long = different / minutesInMilli
            different %= minutesInMilli

            val elapsedSeconds: Long = different / secondsInMilli
            val json = JSONObject()
            println(elapsedHours)
            println(elapsedMinutes)
            println(elapsedSeconds)
            json.put("hours", elapsedHours.toString().padStart(2, '0'))
            json.put("minutes", elapsedMinutes.toString().padStart(2, '0'))
            json.put("seconds", elapsedSeconds.toString().padStart(2, '0'))

            return json
        }
    }
}

