package com.project.android_absent.endpoint

object Endpoints {
    val BASE_URL = "http://192.168.0.106/backend/"

    val LOGIN = BASE_URL+"user/user_login.php"
    val USER_LIST = BASE_URL+"user/user_limit.php"
    val USER_DETAIL = BASE_URL+"user/user_filter.php"
    val USER_REGISTER = BASE_URL+"user/user_input.php"
    val USER_DELETE = BASE_URL+"user/user_delete.php"
    val USER_STATUS = BASE_URL+"status/status.php"
    val USER_JOBS = BASE_URL+"jobs/jobs.php"
    val USER_JOBS_ID = BASE_URL+"jobs/jobs_id.php"
    val USER_DAYS = BASE_URL+"days/days.php"
    val USER_STATUS_ID = BASE_URL+"status/status_id.php"
    val USER_ABSENT = BASE_URL+"absent/absent_filter.php"
    val USER_LOCATION = BASE_URL+"location/location.php"
    val USER_ABSENT_IN = BASE_URL+"absent/absent_in.php"
    val USER_ABSENT_IN_ = BASE_URL+"absent/absent_in_.php"
    val USER_ABSENT_OUT = BASE_URL+"absent/absent_out.php"
    val USER_ABSENT_LIST = BASE_URL+"absent/absent.php"
    val USER_ABSENT_ALL = BASE_URL+"absent/absent_all.php"
    val LOCATION = BASE_URL+"job_location/job_location.php"
    val USER_LOCATION_INPUT= BASE_URL+"job_location/job_location_input.php"
    val LOCATION_DETAIL = BASE_URL+"job_location/job_location_detail.php"
}