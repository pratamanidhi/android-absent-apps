package com.project.android_absent.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.project.android_absent.R
import com.project.android_absent.Util.Util
import com.project.android_absent.endpoint.Endpoints
import com.project.android_absent.model.AbsentModel
import com.project.android_absent.model.EmployeeModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.list_absent.view.*

import org.json.JSONException
import org.json.JSONObject

class AbsentAdapter (val context: Context, val datas : ArrayList<AbsentModel>) : RecyclerView.Adapter<AbsentAdapter.ViewHolder>(){
    var job : String = ""
    override fun getItemCount(): Int {
        return datas.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AbsentAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.list_absent, parent, false))
    }

    override fun onBindViewHolder(holder: AbsentAdapter.ViewHolder, position: Int) {
        val data = datas[position]
        val id = data.id

        val userObj = JSONObject()
        userObj.put("id", id)
        val que = Volley.newRequestQueue(context)
        val req = JsonObjectRequest(Request.Method.POST, Endpoints.USER_DETAIL, userObj,{
            response->
            try {
                Log.e("mess", response.toString())
                val data = response.getJSONObject("data")
                val name = data.getString("nama")
                val job = data.getString("user_job")
                holder.name.text = name
                userJob(job, holder.job, holder.salary)
            }catch (e : JSONException){
                Log.e("ERROR", e.toString())
            }
        },{
            error->
            Log.e("ERROR", error.toString())
        })
        que.add(req)



        holder.ins.text = data.ins
        holder.out.text = data.out
    }

    private fun userJob(id : String, textView1: TextView, textView2: TextView){
        val obj = JSONObject()
        obj.put("id",id)
        val que = Volley.newRequestQueue(context)
        val req = JsonObjectRequest(Request.Method.POST, Endpoints.USER_JOBS_ID, obj, {
                response ->
            try {
                Log.e("mess", response.toString())
                val data = response.getJSONObject("data")
                val jobs = data.getString("pekerjaan")
                val salary = data.getString("gaji")
                textView1.text = jobs
                textView2.text = Util.IDRFormatter(salary.toDouble())
            }catch (e : JSONException){
                Log.e("ERROR", e.toString())
            }
        },{
                error ->
            Log.e("ERROR", error.toString())
        })
        que.add(req)
    }


    inner class ViewHolder(view : View) : RecyclerView.ViewHolder(view){
        val name = view.edt_name
        val job = view.edt_jobs
        val ins = view.edt_in
        val out = view.edt_out
        val salary = view.tv_salary
    }

}