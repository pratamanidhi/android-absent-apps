package com.project.android_absent.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.project.android_absent.R
import com.project.android_absent.endpoint.Endpoints
import com.project.android_absent.model.EmployeeModel
import kotlinx.android.synthetic.main.list_employee.view.*
import org.json.JSONException
import org.json.JSONObject

class EmployeeAdapter(val context: Context, val datas : ArrayList<EmployeeModel>) : RecyclerView.Adapter<EmployeeAdapter.ViewHolder>(){
    override fun getItemCount(): Int {
        return datas.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmployeeAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.list_employee, parent, false))
    }

    override fun onBindViewHolder(holder: EmployeeAdapter.ViewHolder, position: Int) {
        val data = datas[position]
        holder.name.text = data.name
        val obj = JSONObject()
        obj.put("id", data.job)
        val que = Volley.newRequestQueue(context)
        val req = JsonObjectRequest(Request.Method.POST, Endpoints.USER_JOBS_ID, obj, {
            response ->
            try {
                val data = response.getJSONObject("data")
                val job = data.getString("pekerjaan")
                holder.job.text = job
            }catch (e : JSONException){
                Log.e("ERROR", e.toString())
            }
        },{
            error ->
            Log.e("ERROR", error.toString())
        })
        que.add(req)

        holder.delete.setOnClickListener {
            delete(data.id)
        }

    }

    private fun delete( id : String){
        val obj = JSONObject()
        obj.put("id", id)
        Log.e("mess", obj.toString())
        val que = Volley.newRequestQueue(context)
        val req = JsonObjectRequest(Request.Method.POST, Endpoints.USER_DELETE, obj, {
            response ->
            try {
                Log.e("mess", response.toString())
            }catch (e : JSONException){
                Log.e("ERRROR", e.toString())
            }
        },{
            error ->
            Log.e("ERROR", error.toString())
        })
        que.add(req)
    }

    inner class ViewHolder(view : View) : RecyclerView.ViewHolder(view){
        val name = view.edt_name
        val job = view.edt_jobs
        val delete = view.btn_delete
    }

}