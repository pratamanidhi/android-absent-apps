package com.project.android_absent.model

data class AbsentModel(
    val id : String = "",
    val name : String = "",
    val jobs : String = "",
    val ins : String = "",
    val out : String = ""
)