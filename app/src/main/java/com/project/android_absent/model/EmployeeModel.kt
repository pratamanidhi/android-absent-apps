package com.project.android_absent.model

data class EmployeeModel(
        val id : String = "",
        val name : String = "",
        val job : String = ""
)
