package com.project.android_absent.model

data class AdminModel(
        val id : String = "",
        val name : String = "",
        val job : String = ""
)
