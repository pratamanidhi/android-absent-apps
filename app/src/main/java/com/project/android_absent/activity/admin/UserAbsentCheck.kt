package com.project.android_absent.activity.admin

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.project.android_absent.R
import com.project.android_absent.adapter.AbsentAdapter
import com.project.android_absent.endpoint.Endpoints
import com.project.android_absent.model.AbsentModel
import kotlinx.android.synthetic.main.activity_absent_list.*
import org.json.JSONException
import org.json.JSONObject

class UserAbsentCheck : AppCompatActivity() {
    lateinit var context: Context
    lateinit var absentAdapter : AbsentAdapter
    var absent : ArrayList<AbsentModel> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin_user)
        context = this
        getAbsent()
    }
    private fun getAbsent(){
        val que = Volley.newRequestQueue(this)
        val req = JsonObjectRequest(Request.Method.GET, Endpoints.USER_ABSENT_ALL, null, {
                response ->
            try {
                val data = response.getJSONArray("data")
                for (i in 0 until data.length()){
                    val item = data.getJSONObject(i)
                    val nip = item.getString("nip")
                    val ins = item.getString("jam_masuk")
                    val out = item.getString("jam_keluar")
                    absent.add(
                        AbsentModel(
                            nip,
                            "aaa",
                            "bbbb",
                            ins,
                            out
                        )
                    )
                    rv_absent.layoutManager = LinearLayoutManager(context)
                    absentAdapter = AbsentAdapter(context, absent)
                    rv_absent.adapter = absentAdapter
                }
            }catch (e : JSONException){
                Log.e("ERROR", e.toString())
            }
        },{
                error ->
            Log.e("ERROR", error.toString())
        })
        que.add(req)
    }
}