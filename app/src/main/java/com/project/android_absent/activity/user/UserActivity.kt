package com.project.android_absent.activity.user
import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.libraries.places.api.Places
import com.project.android_absent.R
import com.project.android_absent.endpoint.Endpoints
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONException
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class UserActivity : AppCompatActivity(), OnMapReadyCallback {

    private val pERMISSION_ID = 42
    lateinit var mFusedLocationClient: FusedLocationProviderClient
    lateinit var mMap: GoogleMap
    var LAT : String = ""
    var LON : String = ""

    var currentLocation: LatLng = LatLng(20.5, 78.9)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val ai: ApplicationInfo = applicationContext.packageManager
            .getApplicationInfo(applicationContext.packageName, PackageManager.GET_META_DATA)
        val value = ai.metaData["AIzaSyD5T4tCGAO4eBRNmw93xUXx0Jdq5lbDF_g"]
        val apiKey = value.toString()
        if (!Places.isInitialized()) {
            Places.initialize(applicationContext, apiKey)
        }
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        val sdf = SimpleDateFormat("dd/M/yyyy HH:mm:ss")
        val currentDate = sdf.format(Date())
        Log.e("date", currentDate)

        val id = intent.getStringExtra("id")
        Log.e("mess", id!!)
        getLastLocation()
        if (id != null) {
            userDetail(id)
            isAbsent(id, currentDate)
            btn_out.visibility = View.GONE
            btn_in.setOnClickListener {
                locationRecord(id,"in", currentDate)
                isIn(id, null, false)
            }
            btn_data.setOnClickListener {
                val intent = Intent(this, UserAbsentActivity::class.java)
                intent.putExtra("id", id)
                startActivity(intent)
            }
        }
    }


    override fun onMapReady(p0: GoogleMap) {
        mMap = p0
        getLastLocation()
    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient.lastLocation.addOnCompleteListener(this) { task ->
                    val location: Location? = task.result
                    if (location == null) {
                        requestNewLocationData()
                    } else {
                        currentLocation = LatLng(location.latitude, location.longitude)
                        LAT = location.latitude.toString()
                        LON = location.longitude.toString()
                        Log.e("mess", LAT.toString() + " / " + LON.toString())
                        mMap.clear()
                        mMap.addMarker(MarkerOptions().position(currentLocation))
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 16F))
                    }
                }
            } else {
                Toast.makeText(this, "Turn on location", Toast.LENGTH_LONG).show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        } else {
            requestPermissions()
        }
    }

    @SuppressLint("MissingPermission")
    private fun requestNewLocationData() {
        val mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mFusedLocationClient.requestLocationUpdates(
            mLocationRequest, mLocationCallback,
            Looper.myLooper()
        )
    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val mLastLocation: Location = locationResult.lastLocation
            currentLocation = LatLng(mLastLocation.latitude, mLastLocation.longitude)
        }
    }

    private fun isLocationEnabled(): Boolean {
        val locationManager: LocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    private fun checkPermissions(): Boolean {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION),
            pERMISSION_ID
        )
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == pERMISSION_ID) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                getLastLocation()
            }
        }
    }

    private fun userDetail(id : String){
        val obj = JSONObject()
        obj.put("id", id)
        val que = Volley.newRequestQueue(this)
        val req = JsonObjectRequest(Request.Method.POST, Endpoints.USER_DETAIL, obj,{
            response ->
            try {
                Log.e("mess", response.toString())
                val data = response.getJSONObject("data")
                val name = data.getString("nama")
                val user_status = data.getString("user_status")
                val user_job = data.getString("user_job")
                val location_id = data.getString("job_locations_id")
                locationDetail(location_id)
                userStatus(user_status)
                userJob(user_job)
                edt_name.text = name
            }catch (e : JSONException){
                Log.e("ERROR", e.toString())
            }
        },{
            error ->
            Log.e("ERROR", error.toString())
        })
        que.add(req)
    }

    private fun userStatus(id : String){
        val obj = JSONObject()
        obj.put("id",id)
        val que = Volley.newRequestQueue(this)
        val req = JsonObjectRequest(Request.Method.POST, Endpoints.USER_STATUS_ID, obj, {
            response ->
            try {
                val data = response.getJSONObject("data")
                val status = data.getString("status")
                edt_account.text = status
            }catch (e : JSONException){
                Log.e("ERROR", e.toString())
            }
        },{
            error ->
            Log.e("ERROR", error.toString())
        })
        que.add(req)
    }

    private fun userJob(id : String){
        val obj = JSONObject()
        obj.put("id",id)
        val que = Volley.newRequestQueue(this)
        val req = JsonObjectRequest(Request.Method.POST, Endpoints.USER_JOBS_ID, obj, {
            response ->
            try {
                Log.e("mess", response.toString())
                val data = response.getJSONObject("data")
                val jobs = data.getString("pekerjaan")
                edt_job.text = jobs
            }catch (e : JSONException){
                Log.e("ERROR", e.toString())
            }
        },{
            error ->
            Log.e("ERROR", error.toString())
        })
        que.add(req)
    }

    private fun locationDetail(id : String){
        val obj = JSONObject()
        obj.put("id",id)
        val que = Volley.newRequestQueue(this)
        val req = JsonObjectRequest(Request.Method.POST, Endpoints.LOCATION_DETAIL, obj, {
            response ->
            try {
                val data = response.getJSONObject("data")
                val lat = data.getString("lat")
                val lon = data.getString("long")
                if (LAT != lat && LON != lon){
                    Toast.makeText(this, "Anda tidak dapat melakukan absen, Pastikan anda di lokasi pekerjaan", Toast.LENGTH_LONG).show()
                    btn_in.visibility = View.GONE
                    btn_out.visibility = View.GONE
                }else{
                    Toast.makeText(this, "Anda sedang berada di lokasi pekerjaan", Toast.LENGTH_LONG).show()
                }
            }catch (e : JSONException){
                Log.e("mess", e.toString())
            }
        },{
            error ->
            Log.e("mess", error.toString())
        })
        que.add(req)
    }

    private fun isAbsent(id : String, date : String){
        val obj = JSONObject()
        obj.put("id", id)
        val que = Volley.newRequestQueue(this)
        val req = JsonObjectRequest(Request.Method.POST, Endpoints.USER_ABSENT, obj, {
            response ->
            try {
                Log.e("mess", response.toString())
                val data =  response.getJSONObject("data")
                val idabsen = data.getString("idabsen")
                val ins = data.getString("jam_masuk")
                val out = data.getString("jam_keluar")
                edt_in.text = ins
                edt_out.text = out
                val is_in = data.getString("is_in")
                if (is_in == "0"){
                    btn_in.visibility = View.VISIBLE
                    btn_out.visibility = View.GONE
                    btn_in.setOnClickListener {
                        locationRecord(id,"in", date)
                        isIn(id, date, true)
                        Log.e("in_data", idabsen +" / "+ date)
                    }
                }else if (is_in == "1"){
                    btn_in.visibility = View.GONE
                    btn_out.visibility = View.VISIBLE
                    btn_out.setOnClickListener {
                        locationRecord(id,"out", date)
                        isOut(idabsen, date, true)
                    }
                }
            }catch (e : JSONException){
                Log.e("ERROR", e.toString())
            }

        },{
            error ->
            Log.e("ERROR", error.toString())
        })
        que.add(req)
    }

    private fun locationRecord(id : String, kind : String, date : String){
        val obj = JSONObject()
        obj.put("user_id", id)
        obj.put("lat", LAT)
        obj.put("lon", LON)
        obj.put("kind", kind)
        obj.put("date", date)

        val que = Volley.newRequestQueue(this)
        val req = JsonObjectRequest(Request.Method.POST, Endpoints.USER_LOCATION, obj, {
            response ->
            try {
                Log.e("mess", response.toString())
            }catch (e : JSONException){
                Log.e("error", e.toString())
            }
        },{
            error ->
            Log.e("error", error.toString())
        })
        que.add(req)
    }

    private fun isIn(id: String, absent_in: String?, is_in: Boolean){
        val obj = JSONObject()
        obj.put("id", id)
        obj.put("in", absent_in)
        obj.put("is_in", is_in)
        Log.e("mess", obj.toString())
        Log.e("URL",Endpoints.USER_ABSENT_IN)

        val que = Volley.newRequestQueue(this)
        val req = JsonObjectRequest(Request.Method.POST, Endpoints.USER_ABSENT_IN, obj,{
            response ->
            try {
                Log.e("mess", response.toString())
            }catch (e : JSONException){
                Log.e("error", e.toString())
            }
        },{
            error ->
            Log.e("error", error.toString())
        })
        que.add(req)
    }

    private fun isIn_(id : String, absent_in : String, is_in : Boolean){
        val obj = JSONObject()
        obj.put("id", id)
        obj.put("in", absent_in)
        obj.put("is_in", is_in)
        Log.e("obj", obj.toString())
        Log.e("url",Endpoints.USER_ABSENT_IN_)

        val que = Volley.newRequestQueue(this)
        val req = JsonObjectRequest(Request.Method.POST, Endpoints.USER_ABSENT_IN_, obj,{
                response ->
            try {
                Log.e("mess", response.toString())
            }catch (e : JSONException){
                Log.e("error", e.toString())
            }
        },{
                error ->
            Log.e("error", error.toString())
        })
        que.add(req)
    }

    private fun isOut(id : String, absent_out : String, is_out : Boolean){
        val obj = JSONObject()
        obj.put("id", id)
        obj.put("out", absent_out)
        obj.put("is_out", is_out)
        Log.e("obj", obj.toString())
        Log.e("url",Endpoints.USER_ABSENT_OUT)

        val que = Volley.newRequestQueue(this)
        val req = JsonObjectRequest(Request.Method.POST, Endpoints.USER_ABSENT_OUT, obj,{
            response ->
            try {
                Log.e("mess", response.toString())
            }catch (e : JSONException){
                Log.e("error", e.toString())
            }
        },{
            error ->
            Log.e("error", error.toString())
        })
        que.add(req)
    }
}