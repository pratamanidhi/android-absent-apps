package com.project.android_absent.activity.admin

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.project.android_absent.R
import com.project.android_absent.adapter.AdminAdapter
import com.project.android_absent.adapter.EmployeeAdapter
import com.project.android_absent.endpoint.Endpoints
import com.project.android_absent.model.AdminModel
import com.project.android_absent.model.EmployeeModel
import kotlinx.android.synthetic.main.activity_admin.*
import kotlinx.android.synthetic.main.activity_location_dialog.view.*
import org.json.JSONException
import org.json.JSONObject

class AdminActivity  : AppCompatActivity(){
    lateinit var context: Context
    lateinit var employeeAdapter : EmployeeAdapter
    lateinit var adminAdapter: AdminAdapter
    var employee : ArrayList<EmployeeModel> = ArrayList()
    var admin : ArrayList<AdminModel> = ArrayList()
    private val handler = Handler()
    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var lastLocation: Location? = null
    private var latitudeLabel: String? = null
    private var longitudeLabel: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin)
        context = this
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
//        content()
        getEmployee()
        getAdmin()
        btnAdd()
        if (!checkPermissions()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions()
            }
        }
        else {
            getLastLocation()
        }
    }

    private fun btnAdd(){
        btn_user_add.setOnClickListener {
            startActivity(Intent(this,UserAddActivity:: class.java))
        }
        btn_user_absent.setOnClickListener {
            startActivity(Intent(this,UserAbsentCheck::class.java))
        }
        btn_location_add.setOnClickListener {
            val mDialogView = LayoutInflater.from(this).inflate(R.layout.activity_location_dialog, null)
            val mBuilder = AlertDialog.Builder(this)
                .setView(mDialogView)
                .setTitle("Lokasi")
            val  mAlertDialog = mBuilder.show()
            mDialogView.dialogLoginBtn.setOnClickListener {
                mAlertDialog.dismiss()
                val name = mDialogView.dialogNameEt.text.toString()
                Log.e("mess", name)
                locationInput(name, latitudeLabel!!, longitudeLabel!!)
            }
            mDialogView.dialogCancelBtn.setOnClickListener {
                mAlertDialog.dismiss()
            }
        }
    }

    private fun content(){
        getEmployee()
        getAdmin()
        refresh(10000)
    }

    private fun refresh(i :Int){
        val runnable = object : Runnable{
            override fun run() {
                content()
            }
        }
        handler.postDelayed(runnable, i.toLong())
    }


    private fun getEmployee(){
        val obj = JSONObject()
        obj.put("user_status","2")
        Log.e("mee", obj.toString())

        val que = Volley.newRequestQueue(this)
        val req = JsonObjectRequest(Request.Method.POST, Endpoints.USER_LIST, obj, {
            response ->
            Log.e("mee", response.toString())
            try {
                val data = response.getJSONArray("data")
                for (i in 0 until data.length()){
                    val item = data.getJSONObject(i)
                    val id = item.getString("NIP")
                    val name = item.getString("nama")
                    val job = item.getString("user_job")
                    employee.add(
                            EmployeeModel(
                                    id,
                                    name,
                                    job
                            )
                    )
                    rv_employee.layoutManager = LinearLayoutManager(context)
                    employeeAdapter = EmployeeAdapter(context,employee)
                    rv_employee.adapter = employeeAdapter
                }
            }catch (e : JSONException){
                Log.e("mee", e.toString())
            }
        },{
            error ->
            Log.e("mee", error.toString())
        })
        que.add(req)
    }

    private fun getAdmin(){
        val obj = JSONObject()
        obj.put("user_status","1")
        Log.e("mee", obj.toString())

        val que = Volley.newRequestQueue(this)
        val req = JsonObjectRequest(Request.Method.POST, Endpoints.USER_LIST, obj, {
            response ->
            Log.e("mee", response.toString())
            try {
                val data = response.getJSONArray("data")
                for (i in 0 until data.length()){
                    val item = data.getJSONObject(i)
                    val id = item.getString("NIP")
                    val name = item.getString("nama")
                    val job = item.getString("user_job")
                    admin.add(
                            AdminModel(
                                    id,
                                    name,
                                    job
                            )
                    )
                    rv_admin.layoutManager = LinearLayoutManager(context)
                    adminAdapter = AdminAdapter(context,admin)
                    rv_admin.adapter = adminAdapter
                }
            }catch (e : JSONException){
                Log.e("mee", e.toString())
            }
        },{
            error ->
            Log.e("mee", error.toString())
        })
        que.add(req)
    }

    private fun locationInput(name : String, lat : String, lon : String){
        val obj = JSONObject()
        obj.put("location", name)
        obj.put("lat", lat)
        obj.put("lon", lon)
        val que = Volley.newRequestQueue(this)
        val req = JsonObjectRequest(Request.Method.POST, Endpoints.USER_LOCATION_INPUT, obj,{
            response ->
            try {
                Log.e("mess", response.toString())
                Toast.makeText(this, "Data berhasil dimasukan", Toast.LENGTH_LONG).show()
            }catch (e : JSONException){
                Log.e("mess", e.toString())
                Toast.makeText(this, "Data gagal dimasukan", Toast.LENGTH_LONG).show()
            }
        },{
            error ->
            Log.e("mess", error.toString())
            Toast.makeText(this, "Data gagal dimasukan", Toast.LENGTH_LONG).show()
        })
        que.add(req)
    }

    private fun getLastLocation() {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        fusedLocationClient?.lastLocation!!.addOnCompleteListener(this) { task ->
            if (task.isSuccessful && task.result != null) {
                lastLocation = task.result
                latitudeLabel = lastLocation!!.latitude.toString()
                longitudeLabel = lastLocation!!.longitude.toString()
                Log.e("lat", lastLocation!!.latitude.toString())
                Log.e("lon", lastLocation!!.longitude.toString())
            }
            else {
                Log.e(TAG, "getLastLocation:exception", task.exception)
                Toast.makeText(this,"No location detected. Make sure location is enabled on the device.", Toast.LENGTH_LONG).show()
            }
        }
    }
    private fun showSnackbar(
        mainTextStringId: String, actionStringId: String,
        listener: View.OnClickListener
    ) {
        Toast.makeText(this, mainTextStringId, Toast.LENGTH_LONG).show()
    }
    private fun checkPermissions(): Boolean {
        val permissionState = ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
        return permissionState == PackageManager.PERMISSION_GRANTED
    }
    private fun startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
            REQUEST_PERMISSIONS_REQUEST_CODE
        )
    }
    private fun requestPermissions() {
        val shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale(
            this,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
        if (shouldProvideRationale) {
            Log.e(TAG, "Displaying permission rationale to provide additional context.")
            showSnackbar("Location permission is needed for core functionality", "Okay",
                View.OnClickListener {
                    startLocationPermissionRequest()
                })
        }
        else {
            Log.e(TAG, "Requesting permission")
            startLocationPermissionRequest()
        }
    }
    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>,
        grantResults: IntArray
    ) {
        Log.e(TAG, "onRequestPermissionResult")
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            when {
                grantResults.isEmpty() -> {
                    Log.e(TAG, "User interaction was cancelled.")
                }
                grantResults[0] == PackageManager.PERMISSION_GRANTED -> {
                    getLastLocation()
                }
                else -> {
                    showSnackbar("Permission was denied", "Settings",
                        View.OnClickListener {
                            val intent = Intent()
                            intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                            val uri = Uri.fromParts(
                                "package",
                                Build.DISPLAY, null
                            )
                            intent.data = uri
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            startActivity(intent)
                        }
                    )
                }
            }
        }
    }
    companion object {
        private val TAG = "LocationProvider"
        private val REQUEST_PERMISSIONS_REQUEST_CODE = 34
    }
}