package com.project.android_absent.activity.admin

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.project.android_absent.R
import com.project.android_absent.endpoint.Endpoints
import kotlinx.android.synthetic.main.activity_admin_add.*
import org.json.JSONException
import org.json.JSONObject

class UserAddActivity : AppCompatActivity() {
    lateinit var context: Context
    lateinit var user_status: Spinner
    lateinit var user_jobs : Spinner
    lateinit var user_days : Spinner
    lateinit var user_location : Spinner
    var user_status_id : String = ""
    var user_job_id : String = ""
    var user_days_id : String = ""
    var user_location_id : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin_add)
        context = this
        userStatus()
        userJob()
        userDays()
        inputUser()
        userLocation()
    }

    private fun inputUser(){
        btn_input.setOnClickListener {
            val name = edt_name.text.toString()
            val address = edt_address.text.toString()
            val phone = edt_phone.text.toString()
            val username = edt_username.text.toString()
            val password = edt_password.text.toString()
            if (name.isEmpty()){
                edt_name.error = getString(R.string.error)
            }else if (address.isEmpty()){
                edt_address.error = getString(R.string.error)
            }else if(phone.isEmpty()){
                edt_phone.error = getString(R.string.error)
            }else if(username.isEmpty()){
                edt_username.error = getString(R.string.error)
            }else if(password.isEmpty()){
                edt_password.error = getString(R.string.error)
            }else{
                userRegister(name, address, phone, username, password)
            }
        }
    }

    private fun userStatus(){
        val que = Volley.newRequestQueue(this)
        val req = JsonObjectRequest(Request.Method.GET, Endpoints.USER_STATUS, null, { response ->
            try {
                val data = response.getJSONArray("data")
                val user = ArrayList<String>()
                for (i in 0 until data.length()) {
                    val item = data.getJSONObject(i)
                    val status = item.getString("status")
                    user.add(status)
                }
                user_status = findViewById(R.id.sp_user_status)
                val adapter = ArrayAdapter(
                    this, android.R.layout.simple_spinner_item, user
                )
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                user_status.adapter = adapter
                user_status.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                        val text = user_status.selectedItem.toString()
                        val content = response.getJSONArray("data")
                        for (i in 0 until content.length()){
                            val item = content.getJSONObject(i)
                            val status = item.getString("status")
                            if (status == text){
                                user_status_id = item.getString("id")
                                Log.e("MESS", user_status_id)
                            }
                        }
                    }
                    override fun onNothingSelected(p0: AdapterView<*>?) {
                        TODO("Not yet implemented")
                    }
                }
            } catch (e: JSONException) {
                Log.e("ERROR", e.toString())
            }
        }, { error ->
            Log.e("ERROR", error.toString())
        })
        que.add(req)
    }

    private fun userJob(){
        val que = Volley.newRequestQueue(this)
        val req = JsonObjectRequest(Request.Method.GET, Endpoints.USER_JOBS, null, {
            response ->
            try {
                val data = response.getJSONArray("data")
                val jobs = ArrayList<String>()
                for (i in 0 until data.length()){
                    val item = data.getJSONObject(i)
                    val name = item.getString("pekerjaan")
                    jobs.add(name)
                }
                user_jobs = findViewById(R.id.sp_user_job)
                val adapter = ArrayAdapter(
                    this, android.R.layout.simple_spinner_item, jobs
                )
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                user_jobs.adapter = adapter
                user_jobs.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                        val text = user_jobs.selectedItem.toString()
                        val content = response.getJSONArray("data")
                        for (i in 0 until content.length()){
                            val item = content.getJSONObject(i)
                            val name = item.getString("pekerjaan")
                            if (name == text){
                                user_job_id = item.getString("id_pekerjaan")
                                Log.e("MESS",user_job_id)
                            }
                        }
                    }
                    override fun onNothingSelected(p0: AdapterView<*>?) {
                        TODO("Not yet implemented")
                    }
                }
            }catch (e : JSONException){
                Log.e("ERROR", e.toString())
            }
        },{
            error ->
            Log.e("ERROR", error.toString())
        })
        que.add(req)
    }

    private fun userDays(){
        val que = Volley.newRequestQueue(this)
        val req = JsonObjectRequest(Request.Method.GET, Endpoints.USER_DAYS, null, {
            response ->
            try {
                val data = response.getJSONArray("data")
                val days = ArrayList<String>()
                for (i in 0 until data.length()){
                    val item = data.getJSONObject(i)
                    val name = item.getString("days")
                    days.add(name)
                }
                user_days = findViewById(R.id.sp_user_days)
                val adapter = ArrayAdapter(
                    this, android.R.layout.simple_spinner_item, days
                )
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                user_days.adapter = adapter
                user_days.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                        val text = user_days.selectedItem.toString()
                        val content = response.getJSONArray("data")
                        for (i in 0 until content.length()){
                            val item = content.getJSONObject(i)
                            val name = item.getString("days")
                            if (name == text){
                                user_days_id = item.getString("id")
                                Log.e("MESS", user_days_id)
                            }
                        }
                    }
                    override fun onNothingSelected(p0: AdapterView<*>?) {
                        TODO("Not yet implemented")
                    }
                }
            }catch (e : JSONException){
                Log.e("ERROR", e.toString())
            }
        },{
            error ->
            Log.e("ERROR", error.toString())
        })
        que.add(req)
    }

    private fun userLocation(){
        val que = Volley.newRequestQueue(this)
        val req = JsonObjectRequest(Request.Method.GET, Endpoints.LOCATION, null, {
            response ->
            try {
                val data = response.getJSONArray("data")
                val days = ArrayList<String>()
                for (i in 0 until data.length()){
                    val item = data.getJSONObject(i)
                    val name = item.getString("location_name")
                    days.add(name)
                }
                user_location = findViewById(R.id.sp_user_location)
                val adapter = ArrayAdapter(
                        this, android.R.layout.simple_spinner_item, days
                )
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                user_location.adapter = adapter
                user_location.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                        val text = user_location.selectedItem.toString()
                        val content = response.getJSONArray("data")
                        for (i in 0 until content.length()){
                            val item = content.getJSONObject(i)
                            val name = item.getString("location_name")
                            if (name == text){
                                user_location_id = item.getString("id")
                                Log.e("MESS", user_location_id)
                            }
                        }
                    }
                    override fun onNothingSelected(p0: AdapterView<*>?) {
                        TODO("Not yet implemented")
                    }
                }
            }catch (e : JSONException){
                Log.e("ERROR", e.toString())
            }
        },{
            error ->
            Log.e("ERROR", error.toString())
        })
        que.add(req)
    }

    private fun userRegister(
        name : String,
        address : String,
        phone : String,
        username : String,
        password : String
    ){
        val obj = JSONObject()
        obj.put("nama", name)
        obj.put("alamat", address)
        obj.put("telepon", phone)
        obj.put("username", username)
        obj.put("password", password)
        obj.put("user_status", user_status_id)
        obj.put("user_job", user_job_id)
        obj.put("hari", user_days_id)
        obj.put("location", user_location_id)

        val que = Volley.newRequestQueue(this)
        val req = JsonObjectRequest(Request.Method.POST, Endpoints.USER_REGISTER, obj, {
            response ->
            try {
                Log.e("mess", response.toString())
                Toast.makeText(this, "Data berhasil dimasukan", Toast.LENGTH_LONG).show()
            }catch (e:JSONException){
                Log.e("ERROR", e.toString())
                Toast.makeText(this, "Data gagal dimasukan", Toast.LENGTH_LONG).show()
            }
        },{
            error ->
            Log.e("ERROR", error.toString())
            Toast.makeText(this, "Data gagal dimasukan", Toast.LENGTH_LONG).show()
        })
        que.add(req)
    }

}