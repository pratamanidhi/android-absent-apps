package com.project.android_absent.activity.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.project.android_absent.R
import com.project.android_absent.activity.admin.AdminActivity
import com.project.android_absent.activity.admin.UserAddActivity
import com.project.android_absent.activity.user.UserActivity
import com.project.android_absent.endpoint.Endpoints
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONException
import org.json.JSONObject

class LoginActivity : AppCompatActivity() {
    lateinit var context: Context
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        context = this
        loginBtn()
    }

    private fun loginBtn(){
        btn_login.setOnClickListener {
            val username = edt_username.text.toString()
            val password = edt_password.text.toString()
            setLogin(username,password)
        }
    }

    private fun setLogin(
        username : String,
        password : String
    ){
        val obj = JSONObject()
        obj.put("username", username)
        obj.put("password", password)
        Log.e("obj", obj.toString())
        Log.e("url",Endpoints.LOGIN)
        val que = Volley.newRequestQueue(this)
        val req = JsonObjectRequest(Request.Method.POST, Endpoints.LOGIN, obj, {
            response ->
            try {
                val data = response.getJSONObject("data")
                val id = data.getString("NIP")
                val user_status = data.getString("user_status")
                if (user_status == "1"){
                    val intent = Intent(this, AdminActivity::class.java)
                    intent.putExtra("id", id)
                    startActivity(intent)
                }else{
                    val intent = Intent(this, UserActivity::class.java)
                    intent.putExtra("id", id)
                    startActivity(intent)
                }
            }catch (e : JSONException){
                Log.e("ERROR", e.toString())
            }
        },{
            error->
            Log.e("ERROR", error.toString())
        })
        que.add(req)
    }
}